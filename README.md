This project:

- Uses a trigger token and CURL to trigger a pipeline in another project.
- Overrides a CI/CD variable in the other project.
- Triggers a pipeline in a second project using a CI_JOB_TOKEN.
- Is included in the CI_JOB_TOKEN allowlist for the second project.
